var express = require('express');
var router = express.Router();

var panier = require('../controllers/panierController');

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Export des paniers PMB Ocim' });
});

router.get('/paniers', panier.caddies);

module.exports = router;
