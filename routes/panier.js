var express = require('express');
var router = express.Router();

var panier = require('../controllers/panierController');

router.get('/:id', panier.notice);

module.exports = router;