try {
    var config = require('../config.js');
} catch (e) {
    throw e;
}
var mysql = require('mysql');
var pool = mysql.createPool(config.db);

pool.getConnection(function (err, connection) {
    if (err) return console.log('err :' + err);

    console.log('connected as id ' + connection.threadId);

    if (connection) connection.release();

    return;
});

module.exports = pool;