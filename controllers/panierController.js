var express = require('express');
var router = express.Router();
try {
    var db = require('./db');
} catch (e) {
    router.use((req, res, next) => {
        return next(e);
    });
}

var panier = {

    notice: (req, res, next) => {
        var sqlFindCaddie = "SELECT name FROM caddie WHERE idcaddie = ? ;";
        db.query(sqlFindCaddie, [req.params.id], (err, rows, fields) => {
            if (err) return next(err);
            if (rows[0]) {
                var caddieName = rows[0].name;
                var sqlFindNotices = "SELECT notices.notice_id, tit1 as `titre`, tit2, tit3, tit4, n.tit_perio as `titreperio`, GROUP_CONCAT(author_name,' ', author_rejete ORDER BY responsability.responsability_type,responsability.responsability_ordre SEPARATOR ' ; ') as auteur,niveau_hierar, bulletin_numero, bulletin_notice, mention_date, publishers.ed_ville as `ed_ville`, publishers.ed_name as `editeur`, collections.collection_name as collection, nocoll, lien, year as `date`, npages as nb_pages, ill as illustration, size as taille, code as isbn, n_resume as resume FROM `notices`  LEFT JOIN caddie_content ON caddie_content.object_id = notices.notice_id LEFT JOIN responsability ON responsability_notice = notices.notice_id     LEFT JOIN authors ON authors.author_id = responsability_author  LEFT JOIN analysis ON analysis.analysis_notice = notices.notice_id  LEFT JOIN bulletins ON analysis.analysis_bulletin = bulletins.bulletin_id LEFT JOIN (SELECT notice_id,tit1 as tit_perio FROM notices) n ON bulletins.bulletin_notice = n.notice_id  LEFT JOIN publishers ON notices.ed1_id = publishers.ed_id  LEFT JOIN collections ON notices.coll_id = collections.collection_id  WHERE caddie_content.caddie_id = ?  GROUP BY notice_id  ORDER BY niveau_hierar, date DESC, titre ;";
                db.query(sqlFindNotices, [req.params.id], (err, rows, fields) => {
                    if (err) return next(err);
                    res.render('panier/single', {
                        title: caddieName,
                        notices: rows
                    });
                });
            }
             else {
                return next();
            }
        });
    },
    caddies: (req, res, next) => {
        var sqlFindCaddies = "SELECT idcaddie,name FROM caddie ORDER BY name;";
        db.query(sqlFindCaddies, (err, rows, fields) => {
            if (err) return next(err);
            res.render('panier/index', {
                caddies: rows
            });
        });
    }
};

module.exports = panier;