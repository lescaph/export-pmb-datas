var config = {};

config.db = {
    host: process.env.PMB_DB_HOST,
    user: process.env.PMB_DB_USER,
    password: process.env.PMB_DB_PASSWORD,
    database: process.env.PMB_DB_DATABASE
};

module.exports = config;